//
//  ArticleServiceProtocol.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/22/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import Foundation
protocol ArticleServiceProtocol {
    func didResponseArticle(articles:[Article])
    func didResponseMessage(message:String)
}
