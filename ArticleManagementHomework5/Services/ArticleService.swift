//
//  ArticleService.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/22/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import Foundation
import Alamofire

class ArticleService {
    var articleGetURL = "http://api-ams.me/v1/api/articles"
    var articlePostURL = "http://api-ams.me/v1/api/articles"
    var imagePostURL = "http://api-ams.me/v1/api/uploadfile/single"
    var articlePutURL = "http://api-ams.me/v1/api/articles/"
    var articleDeleteURL = "http://api-ams.me/v1/api/articles/"
    var delegate:ArticleServiceProtocol?
    var headers = ["Content-Type": "Application/json",
                   "Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]
    //Mark: Get article by pagination and limit
    func getArticles(page:Int,limit:Int)  {
        let get_url = "\(articleGetURL)?page=\(page)&limit=\(limit)"
        
        Alamofire.request(get_url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if let data = response.data {
                var articles = [Article]()
                do {
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: Any]
                    
                    if let jsonArticle = jsonResult["DATA"] as? [AnyObject] {
                        for article in jsonArticle {
                            articles.append(Article(JSON: article as! [String: Any])!)
                        }
                        self.delegate?.didResponseArticle(articles: articles)
                        print("Success")
                    }
                } catch {
                    print("Failed to get data")
                }
            }
        }
    }
    //Mark: insert update article
    func insertUpdateArticle(article:Article,img:Data) {
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(img, withName: "FILE", fileName: ".jpg", mimeType: "image/jpeg")
        }, to: imagePostURL,method: .post,headers:headers) { (encodingResult) in
            switch encodingResult{
            case .success(request: let upload, streamingFromDisk: _, streamFileURL:_):
                upload.responseJSON(completionHandler: { (response) in
                    if let data = try? JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any]{
                        let imageURL = data["DATA"] as! String
                        article.image = imageURL
                        if article.id == 0{
                            self.insertArticle(article: article)
                        }else{
                            self.updateAticle(article: article)
                        }
                    }
                })
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //Mark: add new article
    func insertArticle(article:Article) {
        let newArticle:[String:Any] = [
            "TITLE": article.title ?? "Untitle" ,
            "DESCRIPTION": article.desc ?? "Default description",
            "AUTHOR": 1,
            "CATEGORY_ID": 1,
            "STATUS": "1",
            "IMAGE": article.image!
        ]
        
        //let url = URL(string:articlePostURL)
        Alamofire.request(articlePostURL, method:.post, parameters: newArticle, encoding:JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess{
                self.delegate?.didResponseMessage(message: "Inserted Successfully")
            }
        }
    }
    //Mark: update article
    func updateAticle(article:Article) {
        let newArticle:[String:Any] = [
            "TITLE": article.title ?? "Untitle" ,
            "DESCRIPTION": article.desc ?? "Default description",
            "AUTHOR": 1,
            "CATEGORY_ID": 1,
            "STATUS": "1",
            "IMAGE": article.image!
        ]
        Alamofire.request("\(articlePutURL)\(article.id!)", method: HTTPMethod.put, parameters: newArticle, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess{
                self.delegate?.didResponseMessage(message: "Updated succesfully")
            }
        }
       
    }
    
    //Mark: update article
    func deleteAticle(id:Int) {
        Alamofire.request("\(articleDeleteURL)\(id)", method: HTTPMethod.delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess{
                self.delegate?.didResponseMessage(message: "Delete succesfully")
            }
        }
        
    }
    
}

