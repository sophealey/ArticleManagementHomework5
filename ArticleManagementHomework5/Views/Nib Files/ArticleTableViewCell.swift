//
//  ArticleTableViewCell.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/22/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleTableViewCell: UITableViewCell {
    @IBOutlet weak var articleView: UIView!
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(article:Article){
        self.articleView.layer.cornerRadius = 5
        self.saveButton.layer.cornerRadius = 7
        
        self.titleLabel.text  = article.title
            self.dateLabel.text = article.createdDate
        
        if article.image != nil{
            self.articleImageView.kf.setImage(with: URL(string: article.image!))
        }else{
            self.articleImageView.image = UIImage(named: "NoImage")
        }
    }
    
}
