//
//  ArticleUIView.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/27/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit
@IBDesignable
class ArticleUIView: UIView {
    
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 5)
        UIColor.white.setFill()
        path.fill()
    }
    

}
