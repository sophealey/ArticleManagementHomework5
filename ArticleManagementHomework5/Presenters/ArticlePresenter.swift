//
//  ArticlePresenter.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/22/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import Foundation
class ArticlePresenter{
    var delegate:ArticlePresenterProtocol?
    var articleService:ArticleService?
    
    init() {
        self.articleService = ArticleService()
        self.articleService?.delegate = self
    }
    
    func getArticle(page:Int,limit:Int) {
        self.articleService?.getArticles(page: page, limit: limit)
    }
    func insertUpdateArticle(article:Article,img:Data){
        self.articleService?.insertUpdateArticle(article: article, img: img)
    }
    
    func deleteArticle(id:Int)  {
        self.articleService?.deleteAticle(id: id)
    }
}
extension ArticlePresenter:ArticleServiceProtocol{
    func didResponseMessage(message: String) {
        print(message)
    }
    
    func didResponseArticle(articles: [Article]) {
        self.delegate?.didResponeArticle(articles: articles)
    }
    
}
