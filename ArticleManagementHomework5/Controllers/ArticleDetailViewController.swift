//
//  ArticleDetailViewController.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/26/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit
import Kingfisher
import Photos
class ArticleDetailViewController: UIViewController {

    @IBOutlet var longPressGeture: UILongPressGestureRecognizer!
    @IBOutlet weak var articleDescriptionLabel: UILabel!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    var articleDetail:Article?
    override func viewDidLoad() {
        super.viewDidLoad()
        if articleDetail != nil{
            articleTitleLabel.text = articleDetail?.title
            articleDescriptionLabel.text = articleDetail?.desc
            if articleDetail?.image != nil{
                articleImageView.kf.setImage(with: URL(string:(articleDetail?.image)!))
            }else{
                articleImageView.image = #imageLiteral(resourceName: "NoImage")
            }
        }
//        //Mark: long hold
//        let holdToDelete : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gesture:)))
        longPressGeture.minimumPressDuration = 0.5
        longPressGeture.delegate = self as? UIGestureRecognizerDelegate
        longPressGeture.delaysTouchesBegan = true
        self.articleImageView?.addGestureRecognizer(longPressGeture)
        
    }
    @IBAction func logPresToSaveImage(_ sender: UILongPressGestureRecognizer) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Save Image", style: .default, handler: {action in
            self.checkLibraryPermission()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true)
        
    }
    
    func saveImage()  {
            let imageData = UIImagePNGRepresentation(self.articleImageView.image!)
            let compressImage = UIImage(data: imageData!)
            UIImageWriteToSavedPhotosAlbum(compressImage!, nil, nil, nil)
        
        let alertSucces = UIAlertController(title: "You have saved successfully!", message: nil, preferredStyle: .alert)
        alertSucces.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertSucces, animated: true, completion: nil)
        
        
    }
    func checkLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            self.saveImage()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization( { (newStatus) in
                if newStatus ==  PHAuthorizationStatus.authorized {
                    self.saveImage()
                }
            })
        case .restricted, .denied:
            let denied = UIAlertController(title: "Permission denied", message: "Goto Settings > Privacy > Photos and allow permission.", preferredStyle: .alert)
            denied.addAction(UIAlertAction(title: "Not now", style: .cancel, handler: nil))
            denied.addAction(UIAlertAction(title: "Setting", style: .default, handler: { action in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: nil)}
            }))
            present(denied, animated: true)
        }
    }

}
