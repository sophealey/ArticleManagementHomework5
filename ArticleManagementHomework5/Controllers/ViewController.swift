//
//  ViewController.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/22/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var articleTableView: UITableView!
    var articlePresenter:ArticlePresenter?
    var articles = [Article]()
    var page = 1
    var refreshControl: UIRefreshControl!
    
    var newFetchBool = 0
    var indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //articles = [Article]()
        self.articlePresenter = ArticlePresenter()
        self.articlePresenter?.getArticle(page: 1, limit: 15)
        self.articlePresenter?.delegate = self
        
        //Mark: pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:#selector(refresh), for:UIControlEvents.valueChanged)
        articleTableView.addSubview(refreshControl)
       
    }
    @objc func refresh(sender:AnyObject) {
        page = 1
        self.articlePresenter?.getArticle(page: page, limit: 15)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("ArticleTableViewCell", owner: self, options: nil)?.first as? ArticleTableViewCell
         self.articles[indexPath.row].createdDate = self.articles[indexPath.row].createdDate?.formatDate(getTime: false)
        print(articles[indexPath.row].createdDate!)
        cell?.configureCell(article: articles[indexPath.row])
        return cell!
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        newFetchBool = 0
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        newFetchBool += 1
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate && newFetchBool >= 1 && scrollView.contentOffset.y >= 0 {
            articleTableView.layoutIfNeeded()
            self.page += 1
            self.articleTableView.tableFooterView = indicatorView
            self.articleTableView.tableFooterView?.isHidden = false
            self.articleTableView.tableFooterView?.center = indicatorView.center
            self.indicatorView.startAnimating()
            self.articlePresenter?.getArticle(page: self.page, limit: 15)
            newFetchBool = 0
        }else if !decelerate{
            newFetchBool = 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.performSegue(withIdentifier: "showDetail", sender: self.articles[indexPath.row])
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            let pageEdit = self.storyboard?.instantiateViewController(withIdentifier: "StoryboardDetailPage") as! AddEditViewController
            pageEdit.articleDetail = self.articles[indexPath.row]
            self.navigationController?.pushViewController(pageEdit, animated: true)
        }
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, IndexPath) in
            self.deleteArticle(articleID: self.articles[indexPath.row].id!, index: indexPath.row)
        }
        return [delete,edit]
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail"{
            let pageDetail = segue.destination as! ArticleDetailViewController
            pageDetail.articleDetail = sender as? Article
        }
    }
    func deleteArticle(articleID:Int,index:Int)  {
        let refreshAlert = UIAlertController(title: "", message: "Are you sure want to delete?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (action: UIAlertAction!) in
            self.articlePresenter?.deleteArticle(id: articleID)
            self.articles.remove(at: index)
            self.articleTableView.reloadData()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }


}
extension ViewController:ArticlePresenterProtocol{
    func didResponseMessage(message: String) {
        self.didResponseMessage(message: message)
        //self.articlePresenter?.didResponseMessage(message: message)
    }
    
    func didResponeArticle(articles: [Article]) {
        
        if page == 1 {
            self.articles = []
            self.refreshControl.endRefreshing()
        }
        self.articles += articles
        DispatchQueue.main.async {
            self.articleTableView.reloadData()
            self.indicatorView.stopAnimating()
        }
        self.refreshControl?.endRefreshing()
    }
}
extension String{
    func subString(startIndex: Int, endIndex: Int) -> String {
        let end = (endIndex - self.count) + 1
        let indexStartOfText = self.index(self.startIndex, offsetBy: startIndex)
        let indexEndOfText = self.index(self.endIndex, offsetBy: end)
        let substring = self[indexStartOfText..<indexEndOfText]
        return String(substring)
    }
    
    func formatDate(getTime: Bool) -> String {
        if getTime {
            return "\(self.subString(startIndex: 6, endIndex: 7))-\(self.subString(startIndex: 4, endIndex: 5))-\(self.subString(startIndex: 0, endIndex: 3)) / \(self.subString(startIndex: 8, endIndex: 9)):\(self.subString(startIndex: 10, endIndex: 11))"
        } else {
            return "\(self.subString(startIndex: 6, endIndex: 7))-\(self.subString(startIndex: 4, endIndex: 5))-\(self.subString(startIndex: 0, endIndex: 3))"
        }
    }
}

