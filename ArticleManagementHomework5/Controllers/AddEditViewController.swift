//
//  AddEditViewController.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/24/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import UIKit
import Kingfisher
import Photos
class AddEditViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate {

    @IBOutlet weak var articleDescriptionTextView: UITextView!
    @IBOutlet weak var articleTitleTextField: UITextField!
    @IBOutlet weak var articleImageView: UIImageView!
    
    var articleDetail:Article?
    
    var articlePresenter:ArticlePresenter?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.articlePresenter = ArticlePresenter()
        self.articlePresenter?.delegate = self
        articleDescriptionTextView.delegate = self
        
        if articleDetail != nil {
            articleTitleTextField.text = articleDetail?.title
            articleDescriptionTextView.text = articleDetail?.desc
            if let imageURL = articleDetail?.image{
                articleImageView.kf.setImage(with: URL(string:imageURL))
            }
            //Mark: description placeholder
            self.articleTitleTextField.text = ((articleDetail?.title != "Untitle") ? articleDetail?.title : "" ) ?? ""
            if articleDetail?.desc != "" {
                self.articleDescriptionTextView.text = articleDetail?.desc
            }else{
                articleDescriptionTextView.text = "Description"
                articleDescriptionTextView.textColor = UIColor.lightGray
            }
        }else{
            articleDescriptionTextView.text = "Description"
            articleDescriptionTextView.textColor = UIColor.lightGray
        }
        //Mark: work with auto scroll when keyboard is present
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if articleDescriptionTextView.textColor == UIColor.lightGray {
            articleDescriptionTextView.text = nil
            articleDescriptionTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if articleDescriptionTextView.text.isEmpty {
            articleDescriptionTextView.text = "Description"
            articleDescriptionTextView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func saveArticleAction(_ sender: UIBarButtonItem) {
        let image = UIImageJPEGRepresentation(self.articleImageView.image!, 0.5)
        let article = Article()
        if articleDetail != nil {
            article.id = articleDetail?.id
        }else {
            article.id = 0
        }
        article.title = self.articleTitleTextField.text
        article.desc = self.articleDescriptionTextView.text
        self.articlePresenter?.insertUpdateArticle(article: article, img: image!)
    }
    @IBAction func browseImage(send:UITapGestureRecognizer){
        checkLibraryPermission()
    }
    func browse(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    func checkLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            self.browse()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization( { (newStatus) in
                if newStatus ==  PHAuthorizationStatus.authorized {
                    self.browse()
                }
            })
        case .restricted, .denied:
            let denied = UIAlertController(title: "Permission denied", message: "Goto Settings > Privacy > Photos and allow permission.", preferredStyle: .alert)
            denied.addAction(UIAlertAction(title: "Not now", style: .cancel, handler: nil))
            denied.addAction(UIAlertAction(title: "Setting", style: .default, handler: { action in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: nil)}
            }))
            present(denied, animated: true)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image:UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.articleImageView.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
    //Mark: auto scroll when keyboard is present
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            articleDescriptionTextView.contentInset = UIEdgeInsets.zero
        } else {
            articleDescriptionTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        articleDescriptionTextView.scrollIndicatorInsets = articleDescriptionTextView.contentInset
    
        let selectedRange = articleDescriptionTextView.selectedRange
        articleDescriptionTextView.scrollRangeToVisible(selectedRange)
    }

}
extension AddEditViewController:ArticlePresenterProtocol{
    func didResponeArticle(articles: [Article]) {
        
    }
    func didResponseMessage(message: String) {
        
    }
    
    
}
