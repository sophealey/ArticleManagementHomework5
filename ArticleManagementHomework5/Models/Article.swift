//
//  Article.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/22/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import Foundation
import ObjectMapper

class Article{
    var id:Int?
    var title:String?
    var desc:String?
    var category:Category?
    var createdDate:String?
    var image:String?
    
    init() {
        self.category = Category()
    }
    required init(map:Map) {}
}

extension Article:Mappable{
    
    
    
    func mapping(map: Map) {
        id <- map["ID"]
        title <- map["TITLE"]
        desc <- map["DESCRIPTION"]
        category <- map["CATEGORY"]
        createdDate <- map["CREATED_DATE"]
        image <- map["IMAGE"]
    }
    
    
}
