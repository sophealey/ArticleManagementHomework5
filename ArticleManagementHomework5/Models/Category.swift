//
//  Category.swift
//  ArticleManagementHomework5
//
//  Created by Sophealey on 12/22/17.
//  Copyright © 2017 Sophealey. All rights reserved.
//

import Foundation
import ObjectMapper

class Category{
    var id:Int?
    var name:String?
    init() {
        
    }
    required init(map:Map) {}
}
extension Category:Mappable{
    func mapping(map: Map) {
        id <- map["ID"]
        name <- map["NAME"]
    }
    
    
}
